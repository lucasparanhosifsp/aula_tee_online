import { AngularFireAuth } from '@angular/fire/auth';
import { NavController, ToastController } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  isLoggedIn: Observable<User>;

  constructor(
    private nav: NavController,
    private auth: AngularFireAuth,
    private toast: ToastController
  ) {
    this.isLoggedIn = auth.authState;
  }


  login(user) {

    this.auth.signInWithEmailAndPassword(user.email, user.password)
      .then(() => {
        //Redirect to home
        this.nav.navigateForward("home")
      }).catch(error => {
        //Show error message
        this.showError("Usuário ou senha inválidos");
      });
  }


  private async showError(message) {
    const ctrl = await this.toast.create({
      message: message,
      duration: 5000,
      color: "danger"
    });

    ctrl.present();

  }


  createUser(user) {
    this.auth.createUserWithEmailAndPassword(user.email, user.password)
      .then(() =>
        //Redirect to home
        this.nav.navigateForward("home")
      ).catch(error => {
        //Show error message
        this.showError("Algo deu errado, por favor tente novamente");
      });
  }


  recoverPassword(email) {
    this.auth.sendPasswordResetEmail(email)
      .then(() => this.nav.navigateRoot('auth'))
      .catch(error => {
        //Show error message
        this.showError("Email não encontrado");
      });
  }


  logout() {
    this.auth.signOut()
      .then(() => this.nav.navigateRoot('auth'));
  }

}
