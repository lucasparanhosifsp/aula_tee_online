import { LoginService } from '../../services/login.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  registerForm: FormGroup;

  constructor(
    private builder: FormBuilder,
    private service: LoginService) { }

  ngOnInit() {
    this.registerForm = this.builder.group({
      name: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(19)]],
      surname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(30)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      confirm_password: ['', [Validators.required, Validators.minLength(8)]]
    });
  }

  createUser() {
    const user = this.registerForm.value;

    this.service.createUser(user);
  }


}
