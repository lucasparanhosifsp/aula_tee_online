import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class BillsService {

  collection: AngularFirestoreCollection;

  constructor(
    private db: AngularFirestore
  ) {
  }

  createBill(bill) {
    const id = this.db.createId();
    bill.id = id;

    this.collection = this.db.collection('bills');
    return this.collection.doc(id).set(bill);
  }

  filter(filed, value){
    this.collection = this.db.collection('bills', ref => ref.where(filed, '==', value));
    return this.collection.valueChanges();
  }

  getAll(){    
    this.collection = this.db.collection('bills');
    return this.collection.valueChanges();
  }

  remove(bill){
    this.collection = this.db.collection('bills');
    this.collection.doc(bill.id).delete();
  }
}
