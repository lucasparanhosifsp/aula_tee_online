import { NavController } from '@ionic/angular';
import { BillsService } from './../../services/bills.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'bill',
  templateUrl: './bill.page.html',
  styleUrls: ['./bill.page.scss'],
})
export class BillPage implements OnInit {
  billForm: FormGroup

  constructor(
    private builder: FormBuilder,
    private billsService: BillsService,
    private nav: NavController
  ) { }

  ngOnInit() {
    this.initForm();
  }

  private initForm() {
    this.billForm = this.builder.group({
      partner: ['', [Validators.required, Validators.minLength(3)]],
      description: ['', [Validators.required, Validators.minLength(6)]],
      value: ['', [Validators.required, Validators.min(0.01)]],
      type: ['', [Validators.required]],
      date: ['', [Validators.required]]
    });
  }

  /*
  * Create a new bill on Firebase, in collection "bill"
  */
  createBill(){
    const data = this.billForm.value;
    this.billsService.createBill(data)
      .then(() => this.nav.navigateForward('bills/report'));
  }
}
