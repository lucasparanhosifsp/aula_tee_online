import { BillsService } from './../../services/bills.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'report',
  templateUrl: './report.page.html',
  styleUrls: ['./report.page.scss'],
})
export class ReportPage implements OnInit {

  bills;
  filterValue;

  constructor(
    private billsService: BillsService
  ) { }

  ngOnInit() {
    this.filter();
  }

  filter() {
    if (this.filterValue) {
      this.billsService.filter('type', this.filterValue).subscribe(x => this.bills = x);
    } else {
      this.billsService.getAll().subscribe(x => this.bills = x);
    }
  }

  remove(bill) {
    this.billsService.remove(bill);
  }
}
