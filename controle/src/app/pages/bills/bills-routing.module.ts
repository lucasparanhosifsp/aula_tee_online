import { BillPage } from './pages/bill/bill.page';
import { CommonModule } from '@angular/common';
import { ReportPage } from './pages/report/report.page';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        redirectTo: 'report',
        pathMatch: 'full'
      }, {
        path: 'report',
        component: ReportPage
      }, {
        path: 'bill',
        component: BillPage
      }
    ]
  }
];


@NgModule({
  declarations: [
    ReportPage,
    BillPage
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule,    
  ],
  exports: [RouterModule]
})
export class BillsRoutingModule { }
